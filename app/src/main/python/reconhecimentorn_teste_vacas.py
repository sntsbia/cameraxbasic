import os
import glob
import _pickle as cPickle
import dlib
import cv2
import numpy as np
from os.path import dirname,join

def mainFunction():

    #Local dos arquivos no sistema
    svm_directory = join(dirname(__file__),"recursos","detector_vacas_faces_v13_cel.svm")
    dat_pontos_directory = join(dirname(__file__),"recursos","detector_vacas_face_5pontos.dat")
    resnet_dat_directory = join(dirname(__file__),"recursos","dlib_face_recognition_resnet_model_v1.dat")
    indices_pickle_directory = join(dirname(__file__),"recursos","indices_rn_vaca_cel.pickle")
    descritores_npy_directory = join(dirname(__file__),"recursos","descritores_rn_vaca_cel.npy")
    img_directory = join(dirname(__file__),"reduzidas")

    detectorFace = dlib.simple_object_detector(svm_directory)
    detectorPontos = dlib.shape_predictor(dat_pontos_directory)
    reconhecimentoFacial = dlib.face_recognition_model_v1(resnet_dat_directory)
    indices = np.load(indices_pickle_directory,allow_pickle=True)
    descritoresFaciais = np.load(descritores_npy_directory)
    limiar = 0.32

    for arquivo in glob.glob(os.path.join(img_directory, "*.jpg")):
        imagem = cv2.imread(arquivo)
        facesDetectadas = detectorFace(imagem, 1)
        for face in facesDetectadas:
            e, t, d, b = (int(face.left()), int(face.top()), int(face.right()), int(face.bottom()))
            pontosFaciais = detectorPontos(imagem, face)
            descritorFacial = reconhecimentoFacial.compute_face_descriptor(imagem, pontosFaciais)
            listaDescritorFacial = [fd for fd in descritorFacial]
            npArrayDescritorFacial = np.asarray(listaDescritorFacial, dtype=np.float64)
            npArrayDescritorFacial = npArrayDescritorFacial[np.newaxis, :]

            distancias = np.linalg.norm(npArrayDescritorFacial - descritoresFaciais, axis=1)
            print("Distâncias: {}".format(distancias))
            minimo = np.argmin(distancias)
            print(minimo)
            distanciaMinima = distancias[minimo]
            print(distanciaMinima)

            if distanciaMinima <= limiar:
                nome = os.path.split(indices[minimo])[1].split(".")[0]
            else:
                nome = ' '

            texto = "{} {:.4f}".format(" ",distanciaMinima)

            print("Foto: ",imagem,"Animal: ",nome,"Texto: ",texto)
