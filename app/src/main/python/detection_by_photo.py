import os
import dlib
import cv2
import glob
from os.path import dirname,join

def mainFunction(imgUri):
    print("URI: ", imgUri)
    filename = join(dirname(__file__),"recursos","detector_vacas_faces_v13_cel.svm")

    detectorRelogio=dlib.simple_object_detector(filename)
    totalDetectados=0
    img = cv2.imread(imgUri)
    print(img)
    objetosDetectados=detectorRelogio(img,1)

    totalDetectados+=len(objetosDetectados)
    for d in objetosDetectados:
        e, t, d, b = (int(d.left()), int(d.top()), int(d.right()), int(d.bottom()))
        cv2.rectangle(img, (e,t), (d, b), (0,0,255), 2)
    if  len(objetosDetectados) < 1:
        print("Nao achei nenhuma face no arquivo " ,imgUri )
    else:
        print("Encontrou: ",objetosDetectados, " faces em ", imgUri)

    print ("\nFaces detectadas no total:",totalDetectados)